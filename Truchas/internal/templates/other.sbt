<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Heat Transfer</Cat>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <AttDef Type="mesh" Label="Mesh" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="coordinate-scale-factor" Label="Coordinate Scale Factor" Version="0" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false">
          <DefaultValue>1.0</DefaultValue>
        </Double>
        <Int Name="exodus-block-modulus" Label="Exodus Block Modulus" Version="0" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false">
          <DefaultValue>10000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="outputs" Label="Outputs" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="start-time" Label="Start Time" Version="0">
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="end-time" Label="End Time" Version="0">
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="output-dt" Label="Output Delta-Time Multiplier" Version="0">
          <DefaultValue>0.1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Group Name="output-times" Label="Additional Output Control" AdvanceLevel="1" Extensible="true" NumberOfRequiredGroups="0">
          <ItemDefinitions>
            <Double Name="time" Label="Output Times" NumberOfRequiredValues="2">
              <ComponentLabels>
                <Label>After time:</Label>
                <Label>Use delta time multiplier:</Label>
              </ComponentLabels>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="physics" Label="Physics" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="fluid-body-force" Label="Fluid Body Force" Version="0" Unique="true" NumberOfRequiredValues="3">
          <Categories>
            <Cat>Fluid Flow</Cat>
          </Categories>
          <ComponentLabels>
            <Label>x:</Label>
            <Label>y:</Label>
            <Label>z:</Label>
          </ComponentLabels>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="physical-constants" Label="Physical Constants" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="absolute-zero" Label="Absolute Zero">
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="stefan-boltzmann" Label="Stefan-Boltzmann Constant">
          <DefaultValue>5.67e-8</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
