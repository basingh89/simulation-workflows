#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================
import os
print 'loading', os.path.basename(__file__)

import sys
import smtk

from cardformat import CardFormat

# ---------------------------------------------------------------------
class Namelist:
  '''Descriptor for Truchas namelist

  Intended as a datastore only.
  Arguably this could be implemented as a namedtuple with default values
  '''
# ---------------------------------------------------------------------
  def __init__(self,
               title,
               att_type=None,
               base_item_path=None,
               custom_method = None,
               if_condition=None):
    '''Information for project file namelist

    Required arguments:
    title: (string) the string written to the Truchas file

    Optional arguments:
    att_type: (string) type of attribute to use
    base_item_path: (string) common path to all card items
    custom_method: (string) name of custom method to call in Writer
    if_condition: (string) only write output if condition is in the
      CardFormat Condition set.
    '''
    self.title = title
    self.att_type = att_type
    self.base_item_path = base_item_path
    self.custom_method = custom_method
    self.if_condition = if_condition
