<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">

  <!-- Category & Analysis specifications -->
  <Categories Default="Heat Transfer">
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
  </Categories>

  <!-- Attribute definitions -->
  <Includes>
    <!-- Note: order is important, e.g., put expressions first -->
    <File>templates/tabular-function.sbt</File>
    <File>templates/material.sbt</File>
    <File>templates/thermal-surface-condition.sbt</File>
    <File>templates/boundary-condition.sbt</File>
    <File>templates/numerics-solver.sbt</File>
    <File>templates/body-source-probe.sbt</File>
    <File>templates/enclosure.sbt</File>
    <File>templates/other.sbt</File>
  </Includes>

  <!-- View specifications -->
  <Views>
    <View Type="Group" Title="SimBuilder" TopLevel="true">
      <Views>
        <View Title="Materials"/>
        <View Title="Surfaces" />
        <View Title="Solver" />
        <View Title="Body" />
        <View Title="Sources" />
        <View Title="Enclosure" />
        <View Title="Other" />
        <View Title="Functions"/>
      </Views>
    </View>

    <View Type="Group" Title="Materials" Style="tiled">
      <Views>
        <View Title="Background Material" />
        <View Title="Material" />
      </Views>
    </View>
    <View Type="Instanced" Title="Background Material">
      <InstancedAttributes>
        <Att Name="Background Material" Type="background-material" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Material">
      <AttributeTypes>
        <Att Type="material"/>
      </AttributeTypes>
    </View>

    <View Type="Group" Title="Surfaces" Style="groupbox">
      <Views>
        <View Title="Thermal Surface Conditions"/>
        <View Title="Fluid Boundary Conditions"/>
      </Views>
    </View>
    <View Type="Attribute" Title="Thermal Surface Conditions">
      <AttributeTypes>
        <Att Type="thermal-surface-condition"/>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Fluid Boundary Conditions">
      <AttributeTypes>
        <Att Type="boundary-condition"/>
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="Solver">
      <InstancedAttributes>
        <Att Name="Numerics" Type="numerics" />
        <Att Name="Solver" Type="solver" />
        <Att Name="Simulation Control" Type="simulation-control" />
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="Body">
      <AttributeTypes>
        <Att Name="body" Type="body" />
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="Sources">
      <AttributeTypes>
        <Att Type="ds-source" />
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="Enclosure">
      <InstancedAttributes>
        <Att Name="Enclosure" Type="enclosure-radiation" />
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="Other" Style="tiled">
      <Views>
        <View Title="Other Instanced" />
        <View Title="Probes" />
      </Views>
    </View>
    <View Type="Instanced" Title="Other Instanced">
      <InstancedAttributes>
        <Att Name="Mesh" Type="mesh" />
        <Att Name="Outputs" Type="outputs" />
        <Att Name="Physics" Type="physics" />
        <Att Name="Physical Constants" Type="physical-constants" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Probes">
      <AttributeTypes>
        <Att Type="probe" />
      </AttributeTypes>
    </View>
    <View Type="SimpleExpression" Title="Functions">
      <Att Type="tabular-function"/>
    </View>
  </Views>

</SMTK_AttributeSystem>
