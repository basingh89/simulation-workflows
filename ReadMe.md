# Simulation Templates for SMTK and CMB

This repository hosts [SMTK]() workflow descriptions describing what inputs
are required to run a variety of open-source simulation packages.
The workflow descriptions include

+ template files holding definitions of physics models, boundary conditions,
  initial conditions, solver termination conditions, and more.
+ export scripts written in Python that take information from a
  filled-out template and generate an input deck for a simulation.

Together, these can be used
in the [CMB]() application suite
or as part of a custom application built on SMTK
to prepare a simulation scenario.

[SMTK]: https://smtk.readthedocs.org/
[CMB]: http://computationalmodelbuilder.org/

## Supported simulations

We currently target the following simulation packages.

+ [Advanced Computational Electromagnetic Simulation Suite (ACE3P)](https://confluence.slac.stanford.edu/display/AdvComp/ACE3P+-+Advanced+Computational+Electromagnetic+Simulation+Suite):
  a set of simulation codes for accelerator and other scientific/engineering
  applications in the areas of parallel electromagnetic modeling and
  parallel beam simulations.
+ [Adaptive Hydraulics (AdH)](http://adh.usace.army.mil/new_webpage/main/main_page.htm):
   a 2d flow simulator aimed at hydrological models.
+ [Albany](https://github.com/gahansen/Albany/wiki):
  a high-performance, parallel, mutliphysics solver.
+ [Dakota](https://dakota.sandia.gov/): a software toolkit that provides a flexible,
  extensible interface between analysis codes and iterative system analysis methods.
+ [Gridded Surface Subsurface Hydrologic Analysis (GSSHA)](http://www.gsshawiki.com/Gridded_Surface_Subsurface_Hydrologic_Analysis):
  a physics-based, distributed, hydrologic, sediment and constituent fate and transport
  modeling and analysis system.
+ [Hydra](http://www.casl.gov/Hydra.shtml):
  a hybrid finite-element/finite-volume incompressible/low-Mach flow solver
  built on the [Hydra toolkit](https://get-hydra.lanl.gov/).
+ [Proteus](http://proteustoolkit.org/):
  a python toolkit for computational methods and simulation.
+ [Truchas](https://github.com/truchas/truchas-release):
  an open source software for 3-D multiphysics simulation of metal casting and processing.
